from typing import Hashable
from aiohttp import web

from handlers import Handlers


class Router:

    def __init__(self, app: web.Application) -> None:
        app.add_routes([
            web.get('/', Handlers().index, name='index'),
            web.get('/version', Handlers().version, name='version'),
            web.get('/json', Handlers().json, name='json'),

            web.post('/add', Handlers().add, name='add'),
        ])
