from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Column
from sqlalchemy.sql.expression import desc
from sqlalchemy.sql.sqltypes import String
from sqlalchemy.types import Integer

DBURI = 'sqlite:///test.db'

Session = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=create_engine(DBURI)
)

session = scoped_session(Session)

Base = declarative_base()

class Note(Base):
    __tablename__ = 'notes'

    id = Column(Integer, primary_key=True)
    title = Column(String(250))
    description = Column(String(250))
    created_at = Column(String(250))
    created_by = Column(String(250))
    priority = Column(Integer)

    def __init__(self, title, description, created_at, created_by, priority) -> None:
        self.title = title
        self.description = description
        self.created_at = created_at
        self.created_by = created_by
        self.priority = priority

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        serializers = ['id', 'title', 'description', 'created_at', 'created_by', 'priority']
        res = {}
        for an in serializers:
            res[an] = getattr(self, an)
        return res

