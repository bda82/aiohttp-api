import os
import sys
from typing import AsyncIterator
import jinja2
import aiohttp_jinja2
import sqlite3
import asyncio
import aiosqlite
from pathlib import Path
from aiohttp.web import Application, run_app

from router import Router


def get_db_path() -> Path:
    here = Path.cwd()
    while not (here / '.git').exists():
        if here == here.parent:
            raise RuntimeError('Cannot found git root dir')
        here = here.parent
    return here / "db.sqlite3"


async def init_db(app: Application) -> AsyncIterator[None]:
    sqlite_db = get_db_path()
    db = await aiosqlite.connect(sqlite_db)
    db.row_factory = aiosqlite.Row
    app["DB"] = db
    yield
    await db.close()


def try_make_db() -> None:
    sqlite_db = get_db_path()
    if sqlite_db.exists():
        return
    
    with sqlite3.connect(str(sqlite_db)) as conn:
        cur = conn.cursor()
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS notes (
            id INTEGER PRIMARY KEY,
            title TEXT,
            description TEXT)
            """
        )
        conn.commit()


def main():
    try_make_db()
    app = Application()
    Router(app)
    app.cleanup_ctx.append(init_db)
    aiohttp_jinja2.setup(
        app, 
        loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), "templates"))
    )
    run_app(app)


if __name__ == '__main__':
    sys.exit(main())