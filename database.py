

from os import stat


class Databse:

    @staticmethod
    async def list(db) -> list:
        notes = []
        async with db.execute(
            """
            SELECT id, title, description FROM notes
            """
        ) as cursor:
            async for row in cursor:
                notes.append({
                    'id': row['id'],
                    'title': row['title'],
                    'description': row['description']
                })
        return notes

    @staticmethod
    async def add(db, title: str, description: str) -> int:
        note_id = 0
        async with db.execute(
            """
            INSERT INTO notes (title, description) VALUES (?, ?)
            """,
            [title, description],
        ) as cursor:
            note_id = cursor.lastrowid
        await db.commit()
        return note_id