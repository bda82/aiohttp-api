from typing import Any, Dict
from aiohttp_jinja2 import render_template
from aiohttp.web import Request, Response, json_response, HTTPFound

from database import Databse


class Handlers:

    @staticmethod
    async def index(request: Request) -> Dict[str, Any]:
        db = request.config_dict["DB"]
        notes = await Databse().list(db)
        context = {
            'notes': notes
        }
        return render_template(
            'index.html',
            request=request,
            context=context
        )

    @staticmethod
    async def add(request: Request) -> Response:
        data = await request.post()
        title = data.get('title')
        description = data.get('description')
        db = request.config_dict["DB"]
        note_id = await Databse().add(db=db, title=title, description=description)
        raise HTTPFound('/')


    @staticmethod
    async def version(request: Request) -> Response:
        return Response(
            text='0.0.1'
        )
    
    @staticmethod
    async def json(request: Request) -> Response:
        return json_response({
            'data': 'Test data'
        })
            